/* eslint-disable @typescript-eslint/no-unsafe-declaration-merging */
import type {ChildProcess} from 'node:child_process';
import {EventEmitter} from 'node:events';
import {WebSocket} from 'ws';
import type Protocol from 'devtools-protocol';
import type ProtocolMapping from 'devtools-protocol/types/protocol-mapping';
import type ProtocolProxyApi from 'devtools-protocol/types/protocol-proxy-api';
import type {Writable} from 'node:stream';
import {Readable} from 'node:stream';
import type {CommandLogLevel} from '@postscriptum.app/cli-common';
import * as util from 'node:util';

interface CdpCommand {
	id: number;
	method: string;
	params?: object;
	sessionId?: string
}

interface CdpResult {
	id: number;
	result: object;
	sessionId?: string;
	error?: CdpResponseError;
}

interface CdpEvent {
	method: string;
	params: object;
	sessionId?: string;
}

interface CdpResponseError {
	code: number;
	message: string;
	data?: string;
}

// Returns the 'on' method of a type if it exists
type WithOn<T> = T extends { on(event: string, listener: (params: object) => void): void } ? T['on'] : never;
// List of EventEmitter method names to map to the same type of the 'on' method
type EventEmitterListenMethods = 'addListener' | 'off' | 'once' | 'prependListener' | 'prependOnceListener' | 'removeListener';
// List of the domains
type Domains = keyof ProtocolProxyApi.ProtocolApi;
// Adds the EventEmitter methods to a domain API
type DomainApi<D extends Domains> = ProtocolProxyApi.ProtocolApi[D] & { [ m in EventEmitterListenMethods ]: WithOn<ProtocolProxyApi.ProtocolApi[D]> }
// Custom protocol API with the enriched domain APIs
export type ProtocolApi = {
	[d in Domains]: DomainApi<d>;
}

type ParamsType<C extends keyof ProtocolMapping.Commands> = ProtocolMapping.Commands[C]['paramsType'][0];
type ReturnType<C extends keyof ProtocolMapping.Commands> = ProtocolMapping.Commands[C]['returnType'];

interface CdpClientOptions {
	logConsole?: typeof console;
	logLevel?: CommandLogLevel;
}

export interface CdpClientEvents extends ProtocolMapping.Events {
	'error': [error: Error];
}

export interface CdpClient extends EventEmitter<CdpClientEvents>, ProtocolApi {
}

/** A bare bones implementation of Chrome DevTools/Debugging Protocol. */
export class CdpClient extends EventEmitter<CdpClientEvents> implements ProtocolApi {
	protected _connection: CdpConnection;
	protected _msgId: number = 0;
	protected _awaitingReply: Map<number, { resolve: (result: object) => void, reject: (error: Error) => void, command: CdpCommand }> = new Map();
	protected _attachedSessions: Map<string, CdpSession> = new Map();
	protected _logConsole: typeof console;
	protected _logLevel: CommandLogLevel;
	connected: Promise<void>;

	static async connect(endPoint: string | ChildProcess, options: CdpClientOptions = {}): Promise<CdpClient> {
		const client = new CdpClient(endPoint, options);
		await client.connected;
		return client;
	}

	constructor(endPoint: string | ChildProcess, {logLevel = 'warn', logConsole = console}: CdpClientOptions = {}) {
		super();
		this._logConsole = logConsole;
		this._logLevel = logLevel;
		this._connection = (typeof endPoint == 'string') ? new CdpWsConnection(endPoint) : new CdpPipeConnection(endPoint);
		this._connection.on('message', this._messageHandler.bind(this));
		this._connection.on('error', (error) => this.emit('error', error));
		this._connection.on('close', () => this.emit('error', new Error(`Unexpected close of the CDP connection.`)));
		this.connected = this._connection.opened;
		return clientProxy(this);
	}

	/** Creates a new session and binds it to a target (which is newly created if not specified). Specify `params` to create a new target, e.g. `{url}` OR `targetId`. Wait for ready before using it. */
	async attachSession(targetId: string): Promise<CdpSession> {
		const session = new CdpSession(this, targetId);
		await session.attached;
		const sessionId = session.id;
		this._attachedSessions.set(sessionId, session);
		session.once('detached', () => this._attachedSessions.delete(sessionId));
		return session;
	}

	/** Send a command not bound to any session (unless `sessionId` is specified). */
	send<C extends keyof ProtocolMapping.Commands>(method: C, params: ParamsType<C> = {}, sessionId?: string): Promise<ReturnType<C>> {
		const id = this._msgId++;
		const command: CdpCommand = {id, method, params, sessionId};
		if (this._logLevel == 'trace') this._logConsole.debug('Outgoing CDP command:', util.inspect(command, {breakLength: Infinity, maxStringLength: 200}));
		this._connection.send(command);
		return new Promise((resolve, reject) => {
			this._awaitingReply.set(command.id, {command, resolve, reject});
		});
	}

	readStream(handle: Protocol.IO.StreamHandle, {offset, chunkSize}: { offset?: number, chunkSize?: number } = {}, sessionId: string = undefined): Readable {
		const self = this;
		return Readable.from((async function* (): AsyncIterable<Buffer> {
			while (true) {
				const {data, base64Encoded, eof} = await self.send('IO.read', {handle, offset, size: chunkSize}, sessionId);
				yield Buffer.from(data, base64Encoded ? 'base64' : undefined);
				if (eof) break;
			}
		})());
	}

	/** Close the connection. */
	close(): Promise<void> {
		this._connection.removeAllListeners();
		return this._connection.close();
	}

	_messageHandler(message: CdpResult | CdpEvent): void {
		if (this._logLevel == 'trace') this._logConsole.debug('Incoming CDP message:', util.inspect(message, {breakLength: Infinity, maxStringLength: 200}));
		if ('id' in message) {
			const {id, result, error} = message;
			const promise = this._awaitingReply.get(id);
			if (!promise) throw new Error(`Id not awaiting result, but got one: '${id}, ${message}`);
			this._awaitingReply.delete(id);
			if (result) promise.resolve(result);
			else promise.reject(new CdpError(error, promise.command));
		} else if ('method' in message) {
			const {method, params, sessionId} = message;
			// TODO The sessionId parameter is not defined in the protocol mapping
			(this.emit as any)(method, params, sessionId);
			if (method == 'Target.detachedFromTarget') {
				const {sessionId: detachedSessionId} = (params as Protocol.Target.DetachedFromTargetEvent);
				const session = this._attachedSessions.get(detachedSessionId);
				session.emit('detached');
				this._attachedSessions.delete(detachedSessionId);
			} else if (sessionId) {
				const session = this._attachedSessions.get(sessionId);
				session.emit(method as keyof ProtocolMapping.Events, params);
			}
		} else {
			this.emit('error', new Error('Message without id or method received: ' + message));
		}
	}
}

class CdpError extends Error {
	code: number;

	constructor({code, message, data}: CdpResponseError, command: CdpCommand) {
		const cause = {...command};
		delete cause.id;
		if (cause.sessionId == undefined) delete cause.sessionId;
		if (data) message += ': ' + data;
		super(message, {cause});
		this.name = this.constructor.name;
		this.code = code;
		delete this.stack;
	}
}

export function exceptionDetailsToError(exceptionDetails: Protocol.Runtime.ExceptionDetails): Error {
	if (exceptionDetails.exception) return (exceptionDetails.exception.description || exceptionDetails.exception.value);
	let message = exceptionDetails.text;
	if (exceptionDetails.stackTrace) {
		for (const callframe of exceptionDetails.stackTrace.callFrames) {
			message += `\n    at ${callframe.functionName || '<anonymous>'} (${callframe.url}:${callframe.lineNumber}:${callframe.columnNumber})`;
		}
	}
	return new Error(message);
}

export interface CdpConnectionEvents {
	'message': [message: CdpResult | CdpEvent];
	'close': [];
	'error': [error: Error];
}

export interface CdpConnection extends EventEmitter<CdpConnectionEvents> {
	send(command: CdpCommand): void;

	close(): Promise<void>;

	opened: Promise<void>;
}

export class CdpWsConnection extends EventEmitter<CdpConnectionEvents> implements CdpConnection {
	protected _ws: WebSocket;
	opened: Promise<void>;

	constructor(webSocketDebuggerUrl: string) {
		super();
		this._ws = new WebSocket(webSocketDebuggerUrl);
		this._ws.on('message', this._onMessage.bind(this));
		this._ws.once('close', () => this.emit('close'));
		this._ws.on('error', (error) => this.emit('error', new Error('An error occured on the WebSocket connection', { cause: error })));
		this.opened = new Promise(resolve => {
			this._ws.once('open', resolve);
		});
	}

	send(command: CdpCommand): void {
		return this._ws.send(JSON.stringify(command));
	}

	protected _onMessage(message: string): void {
		let response: CdpResult | CdpEvent;
		try {
			response = JSON.parse(message) as CdpResult | CdpEvent;
		} catch (e) {
			this.emit('error', new Error(`Invalid JSON message: ${message}`, {cause: e}));
			return;
		}
		this.emit('message', response);
	}

	close(): Promise<void> {
		this._ws.removeAllListeners();
		if (this._ws.readyState == WebSocket.CLOSING || this._ws.readyState == WebSocket.CLOSED) return Promise.resolve();
		return new Promise<void>((resolve, reject) => {
			this._ws.close();
			this._ws.once('error', reject);
			this._ws.once('close', resolve);
		});
	}
}

export class CdpPipeConnection extends EventEmitter<CdpConnectionEvents> implements CdpConnection {
	protected _writeStream: Writable;
	protected _readStream: Readable;
	protected _pendingMessage: string;
	opened: Promise<void>;

	constructor(chromeProcess: ChildProcess) {
		super();
		this._pendingMessage = '';
		this._writeStream = chromeProcess.stdio[3] as Writable;
		this._readStream = chromeProcess.stdio[4] as Readable;

		this._readStream.on('data', this._onData.bind(this));
		this._readStream.on('error', (error) =>  this.emit('error', new Error('An error occured on the CDP read stream', { cause: error })));
		this._writeStream.on('close', () => this.emit('close'));
		this._writeStream.on('error', (error) => this.emit('error', new Error('An error occured on the CDP write stream', { cause: error })));

		this.opened = Promise.resolve();
	}

	send(command: CdpCommand): void {
		this._writeStream.write(JSON.stringify(command));
		this._writeStream.write('\0');
	}

	_onData(data: Buffer): void {
		let start = 0;
		let end = data.indexOf('\0', start);
		while (end !== -1) {
			const message = this._pendingMessage + data.toString('utf-8', start, end);
			this._pendingMessage = '';
			let response: CdpResult | CdpEvent;
			try {
				response = JSON.parse(message) as CdpResult | CdpEvent;
			} catch (e) {
				this.emit('error', new Error(`Invalid JSON message: ${message}`, {cause: e}));
				return;
			}
			this.emit('message', response);
			start = end + 1;
			end = data.indexOf('\0', start);
		}
		this._pendingMessage += data.toString('utf-8', start);
	}

	close(): Promise<void> {
		this._readStream.removeAllListeners();
		this._writeStream.removeAllListeners();
		if (this._writeStream.writableEnded) return Promise.resolve();
		return new Promise<void>((resolve, reject) => {
			this._writeStream.end();
			this._writeStream.on('close', () => console.log('writeStream.close'));
			this._writeStream.destroy();
			this._readStream.destroy();
			this._writeStream.once('error', reject);
			this._writeStream.once('close', resolve);
		});
	}
}

export interface CdpSessionEvents extends ProtocolMapping.Events {
	'detached': []
}

export interface CdpSession extends EventEmitter<CdpSessionEvents>, ProtocolApi {

}

export class CdpSession extends EventEmitter<CdpSessionEvents> {
	protected _client: CdpClient;
	protected _sessionId: string;
	protected _targetId: string;
	attached: Promise<string>;

	get id(): string {
		return this._sessionId;
	}

	get targetId(): string {
		return this._targetId;
	}

	constructor(connection: CdpClient, targetId: string) {
		super();
		this._client = connection;
		this.attached = this._client.send('Target.attachToTarget', {targetId, flatten: true}).then(({sessionId}) => {
			this._targetId = targetId;
			this._sessionId = sessionId;
			return sessionId;
		});
		this.once('detached', () => this._sessionId = null);
		return clientProxy(this);
	}

	/** Send a command which includes the `sessionId` parameter of this session. */
	send<C extends keyof ProtocolMapping.Commands>(method: C, params?: ParamsType<C>): Promise<ReturnType<C>> {
		if (!this._sessionId) throw new Error(`A session that has been detached from its target (or not yet been attached) must not be used. The 'attached' promise and the 'detached' event will help you keep track of this.`);
		return this._client.send(method, params, this._sessionId);
	}

	/** Returns a readable stream (Web API) which can be used to read a CDP stream by its handle. */
	readStream(handle: Protocol.IO.StreamHandle, {offset, chunkSize}: { offset?: number, chunkSize?: number } = {}): Readable {
		return this._client.readStream(handle, {offset, chunkSize}, this._sessionId);
	}

	/** Detach this session from its target. */
	async detach(): Promise<void> {
		this.removeAllListeners();
		return new Promise((resolve, reject) => {
			this.once('detached', resolve);
			this._client.send('Target.detachFromTarget', {sessionId: this._sessionId}).catch(reject);
		});
	}
}

function clientProxy<T extends object>(client: T): T {
	return new Proxy<T>(client, {
		get(target, propertyName: string) {
			if (propertyName in target) return (target as any)[propertyName];
			else return domainProxy(target, propertyName);
		}
	});
}

function domainProxy<T extends object>(client: T, domainName: string): T {
	return new Proxy<T>(Object.create(null), {
		get(target, methodName: string) {
			if (methodName in EventEmitter) {
				return (eventName: string, ...args: any[]) => (client as any)[methodName](`${domainName}.${eventName}` as keyof ProtocolMapping.Events, ...args);
			} else {
				return (...args: any[]) => (client as CdpClient).send(`${domainName}.${methodName}` as keyof ProtocolMapping.Commands, ...args);
			}
		},
	});
}
