import type {ChildProcess, IOType} from "node:child_process";
import {spawn} from "node:child_process";
import path from "node:path";
import os from "node:os";
import fs from "node:fs";

const fsp = fs.promises;

export const chromiumSwitches = [
	'--disable-field-trial-config', // https://source.chromium.org/chromium/chromium/src/+/main:testing/variations/README.md
	'--disable-background-networking',
	'--disable-background-timer-throttling',
	'--disable-backgrounding-occluded-windows',
	'--disable-back-forward-cache', // Avoids surprises like main request not being intercepted during page.goBack().
	'--disable-breakpad',
	'--disable-client-side-phishing-detection',
	'--disable-component-extensions-with-background-pages',
	'--disable-component-update', // Avoids unneeded network activity after startup.
	'--no-default-browser-check',
	'--disable-default-apps',
	'--disable-dev-shm-usage',
	'--disable-extensions',
	// AvoidUnnecessaryBeforeUnloadCheckSync - https://github.com/microsoft/playwright/issues/14047
	// Translate - https://github.com/microsoft/playwright/issues/16126
	// HttpsUpgrades - https://github.com/microsoft/playwright/pull/27605
	// PaintHolding - https://github.com/microsoft/playwright/issues/28023
	// ThirdPartyStoragePartitioning - https://github.com/microsoft/playwright/issues/32230
	// LensOverlay - Hides the Lens feature in the URL address bar. Its not working in unofficial builds.
	// PlzDedicatedWorker - https://github.com/microsoft/playwright/issues/31747
	'--disable-features=ImprovedCookieControls,LazyFrameLoading,GlobalMediaControls,DestroyProfileOnBrowserClose,MediaRouter,DialMediaRouteProvider,AcceptCHFrame,AutoExpandDetailsElement,CertificateTransparencyComponentUpdater,AvoidUnnecessaryBeforeUnloadCheckSync,Translate,HttpsUpgrades,PaintHolding,ThirdPartyStoragePartitioning,LensOverlay,PlzDedicatedWorker',
	'--allow-pre-commit-input',
	'--disable-hang-monitor',
	'--disable-ipc-flooding-protection',
	'--disable-popup-blocking',
	'--disable-prompt-on-repost',
	'--disable-renderer-backgrounding',
	'--force-color-profile=srgb',
	'--metrics-recording-only',
	'--no-first-run',
	'--enable-automation',
	'--password-store=basic',
	'--use-mock-keychain',
	// See https://chromium-review.googlesource.com/c/chromium/src/+/2436773
	'--no-service-autorun',
	'--export-tagged-pdf',
	// https://chromium-review.googlesource.com/c/chromium/src/+/4853540
	'--disable-search-engine-choice-screen',
	// https://issues.chromium.org/41491762
	'--unsafely-disable-devtools-self-xss-warnings',
	// Postscriptum
	"--allow-file-access-from-files",
	// font-render-hinting: cf https://github.com/GoogleChrome/playwright/issues/2410
	// font-render-hinting=none avoids incorrect letter spacing and renders the same line-height on headless and non-headless browser
	"--font-render-hinting=none"
];

export const DEFAULT_CHROMIUM_PORT = 9331;


interface SpawnChromeOptions {
	chromePath: string;
	pipe?: boolean;
	port?: number;
	chromeArgs?: string[];
	tempUserData?: boolean;
	logConsole?: typeof console;
	headless: boolean;
}

export async function spawnChrome({chromePath, port, pipe, chromeArgs = [], tempUserData, logConsole, headless}: SpawnChromeOptions): Promise<ChildProcess | string> {
	const args = [];
	if (pipe) args.push('--remote-debugging-pipe');
	if (port) args.push(`--remote-debugging-port=${port}`);

	let tempUserDataDir: string;
	if (tempUserData) {
		tempUserDataDir = await fsp.mkdtemp(path.join(os.tmpdir(), 'postscriptum-userdata-'));
		args.push(`--user-data-dir=${tempUserDataDir}`);
	}

	if (headless) args.push(`--headless`, '--hide-scrollbars', '--mute-audio');

	const stdio: IOType[] = ['ignore', 'ignore', port ? 'pipe' : 'ignore'];
	if (pipe) stdio.push('pipe', 'pipe');

	const chromeProcess = spawn(chromePath, args.concat(chromeArgs), {stdio});
	chromeProcess.on('close', (code, signal) => {
		if (logConsole && (code || signal)) logConsole.error(new Error(`The browser process did not stop properly (${signal || code})`));
		if (tempUserDataDir) fs.rmSync(tempUserDataDir, {recursive: true});
	});

	return new Promise((resolve, reject) => {
		chromeProcess.once('error', (error) => reject(new Error(`The browser process could not be spawned.`, {cause: error})));

		if (port) {
			chromeProcess.stderr.setEncoding('utf-8');
			let stderr = '';
			chromeProcess.stderr.on('data', (data: string) => {
				stderr += data;
				const listenMatch = stderr.match(/DevTools listening on (.+)/);
				if (listenMatch) resolve(pipe ? chromeProcess : listenMatch[1]);
			});
		} else {
			chromeProcess.once('spawn', () => resolve(chromeProcess));
		}
	});
}

interface VersionMetadata {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Browser: string;
	"Protocol-Version": string;
	"User-Agent": string;
	"V8-Version": string;
	"WebKit-Version": string;
	webSocketDebuggerUrl: string;
}

export async function fetchVersionMetadata(port: number | string): Promise<VersionMetadata> {
	let resp: Response;
	const cdpUrl = typeof port === 'string' && port.startsWith('http:') ? port : `http://localhost:${port}`;
	try {
		resp = await fetch(`${cdpUrl}/json/version`);
	} catch (e) {
		throw new Error(`Unable to fetch the browser version metadata.`, {cause: e});
	}
	if (resp.status == 200) return (await resp.json()) as VersionMetadata;
	throw new Error(`Unable to fetch the browser version metadata (error ${resp.status}).`);
}

