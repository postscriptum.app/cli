export type Binding = "_psLoadPlugin" | "_psLoadHyph" | "_psGetAccessibleString" | "_psGetAccessibleStrings";

type Bindings = { [key in Binding]: (payload: string) => void };

declare global {
	interface Window {
		_postscriptumBindings: {
			callCount: number;
			calls: Record<string, (...args: any[]) => void>;
			add: (bindingName: Binding) => (...args: any) => Promise<any>;
		}
	}
}

window._postscriptumBindings = {
	callCount: 0,
	calls: {},
	add(bindingName: Binding) {
		return function (...args: any[]) {
			return new Promise((resolve) => {
				const callId = window._postscriptumBindings.callCount++;
				window._postscriptumBindings.calls[callId] = resolve;
				(window as unknown as Bindings)[bindingName](JSON.stringify([callId].concat(args)));
			});
		};
	}
};

window.postscriptumBridge = {
	loadPlugin: window._postscriptumBindings.add("_psLoadPlugin"),
	loadHyph: window._postscriptumBindings.add("_psLoadHyph"),
	getAccessibleString: window._postscriptumBindings.add("_psGetAccessibleString"),
	getAccessibleStrings: window._postscriptumBindings.add("_psGetAccessibleStrings")
};
