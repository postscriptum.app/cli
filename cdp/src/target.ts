/* eslint-disable @typescript-eslint/naming-convention */

import type { ErrorResult, PreloadConfig, ProcessResult} from "@postscriptum.app/cli-common";
import {defaultFontFamilies} from "@postscriptum.app/cli-common";
import {createPreloadSource, isBasedOn, loadHyph, loadPlugin, watchCoreScript} from "@postscriptum.app/cli-common";
import {postPDF} from "@postscriptum.app/postpdf";
import type Protocol from "devtools-protocol";
import * as fs from "fs";
import type {CdpClient, CdpSession} from "./cdp";
import { exceptionDetailsToError} from "./cdp";
import type {Writable} from "node:stream";
import {EventEmitter} from "node:events";
import type {Package} from "normalize-package-data";

const fsp = fs.promises;

export interface CdpTargetConfig {
	cdpClient: CdpClient;
	preloadConfig: PreloadConfig;
	pipeConsole: boolean;
	watchScript?: boolean;
	corePath?: string;
	pluginsPaths: string[];
	targetId?: string;
	logConsole?: typeof console;
	isolatedFileAccess?: boolean;
}

export interface CdpTargetEvents {
	'error': [ Error ];
	'close': [];
}

export class CdpTarget extends EventEmitter<CdpTargetEvents> {
	config: CdpTargetConfig;
	client: CdpClient;
	session: CdpSession;
	targetId: string;
	prepared: Promise<void>;

	constructor(config: CdpTargetConfig) {
		super();
		this.config = config;
		this.client = config.cdpClient;
		this.prepared = this._prepare();
	}

	protected async _prepare(): Promise<void> {
		await this.client.connected;

		const {preloadConfig, watchScript, corePath, pipeConsole, pluginsPaths, isolatedFileAccess, targetId} = this.config;
		const logConsole = this.config.logConsole || console;

		const {Target} = this.client;
		this.targetId = targetId;
		if (!this.targetId) ({ targetId: this.targetId } = await Target.createTarget({ url: "about:blank "}));
		Target.once("targetCrashed", ({targetId: crashedTargetId, status, errorCode}) => {
			if (crashedTargetId == targetId) this.emit('error', new Error(`Crash of the browser target (${errorCode}: ${status}).`));
		});
		Target.once('targetDestroyed', ({ targetId: destroyedTargetId }) => {
			if (targetId == destroyedTargetId) this.emit('close');
		});

		this.session = await this.client.attachSession(this.targetId);
		this.session.on('detached', () => {
			this.emit('error', new Error(`The CDP session has been detached from the target.`));
		});
		const {Emulation, Page, Runtime, Console, Browser} = this.session;
		await Promise.all([Page, Runtime, Console].map((domain) => domain.enable()));

		Runtime.on('exceptionThrown', ({exceptionDetails}) => {
			logConsole.error(exceptionDetailsToError(exceptionDetails));
		});

		await Page.setFontFamilies({ fontFamilies: defaultFontFamilies });
		// There are differences on font rendering between PDF generated with or without the headless User-Agent…
		const {userAgent} = await Browser.getVersion();
		await Emulation.setUserAgentOverride({ userAgent: userAgent.replace('Headless', '') });

		const bridgeScript = await fsp.readFile(`${__dirname}/bridge.js`, 'utf-8');
		await Page.addScriptToEvaluateOnNewDocument({source: 'window.exports = {};' + bridgeScript});
		await Promise.all(["_psLoadPlugin", "_psLoadHyph", "_psGetAccessibleString", "_psGetAccessibleStrings"].map((name) => Runtime.addBinding({name})));
		Runtime.on("bindingCalled", async ({name, payload}) => {
			const {result: bindingsObject} = await Runtime.evaluate({expression: `window._postscriptumBindings`});
			const args = JSON.parse(payload) as any[];
			const callId = args.shift();
			let result: any = undefined;
			try {
				if (name == "_psLoadPlugin") {
					const [pluginName] = args;
					const script = await loadPlugin(pluginName, pluginsPaths);
					const {scriptId, exceptionDetails} = await Runtime.compileScript({expression: script, sourceURL: `${pluginName}.js`, persistScript: true});
					if (exceptionDetails) this.emit('error', exceptionDetailsToError(exceptionDetails));
					await Runtime.runScript({scriptId});
				} else if (name == "_psLoadHyph") {
					const [lang] = args;
					result = await loadHyph(lang, corePath);
				} else if (name == "_psGetAccessibleString") {
					const [elemId] = args;
					result = await this.getAccessibleString(elemId);
				} else if (name == "_psGetAccessibleStrings") {
					const [elemIds] = args;
					result = await this.getAccessibleStrings(elemIds);
				}
			} catch (e) {
				logConsole.error(e);
				result = null;
			}
			await Runtime.callFunctionOn({
				objectId: bindingsObject.objectId,
				functionDeclaration: /* language=js */ `function call(callId, result) {
					const call = this.calls[callId];
					delete this.calls[callId];
					call(result)
				}`,
				arguments: [{value: callId}, {value: result}]
			});

		});

		if (isolatedFileAccess) {
			const {Fetch, Target} = this.session;
			let firstRequest = true;
			await Fetch.enable({patterns: [{urlPattern: "file:*"}]});
			Fetch.on("requestPaused", async ({requestId, request}) => {
				const {targetInfo} = await Target.getTargetInfo({});
				if (!firstRequest && !isBasedOn(request.url, targetInfo.url)) {
					await Fetch.failRequest({requestId, errorReason: "AccessDenied"});
					return;
				}
				if (firstRequest) firstRequest = false;
				await Fetch.continueRequest({requestId});
			});
		}

		// Execution of the main script on the web page
		let {identifier: scriptId} = await Page.addScriptToEvaluateOnNewDocument({
			source: await createPreloadSource(preloadConfig)
		});

		if (watchScript && corePath) {
			watchCoreScript(corePath, async (coreScript) => {
				try {
					preloadConfig.coreScript = coreScript;
					await Page.removeScriptToEvaluateOnNewDocument({identifier: scriptId});
					({identifier: scriptId} = await Page.addScriptToEvaluateOnNewDocument({
						source: await createPreloadSource(preloadConfig)
					}));
					await Runtime.evaluate({expression: `console.log("[${new Date().toLocaleTimeString()}] Postscriptum script updated")`});
				} catch (e) {
					logConsole.error(e);
				}
			});
		}

		if (pipeConsole) {
			Console.on("messageAdded", ({message}) => {
				let level = message.level as string;
				if (level == 'warning') level = 'warn';
				if (level in console) (logConsole as any)[level](message.text);
			});
		}
	}

	async close(): Promise<void> {
		const {Target} = this.client;
		await Target.closeTarget({targetId: this.targetId});
	}

	async process(url?: string):
		Promise<ProcessResult> {
		const {Page, Runtime} = this.session;
		if (url) await Page.navigate({url});
		else await Page.reload({});
		await new Promise((resolve) => {
			Page.once('domContentEventFired', resolve);
		});
		const {result, exceptionDetails} = await Runtime.evaluate({expression: `window.postscriptumPreload.done`, awaitPromise: true, returnByValue: true});
		if (exceptionDetails) throw exceptionDetailsToError(exceptionDetails);

		const processResult = result.value as ProcessResult | ErrorResult;
		if ('type' in processResult && processResult.type == 'error') {
			const error = new Error(processResult.message);
			error.stack = processResult.stack;
			throw error;
		}
		const pkgData: Package = require(`${__dirname}/../package.json`);
		(processResult as ProcessResult).info.producer = `Postscriptum/CLI ${pkgData.version}`;
		return processResult as ProcessResult;
	}

	async pdf(outputStream: Writable, result: ProcessResult): Promise<void> {
		const {Page} = this.session;
		const {stream: cdpStream } = await Page.printToPDF({
			preferCSSPageSize: true,
			paperWidth: 8.27,
			paperHeight: 11.7,
			marginTop: 0,
			marginBottom: 0,
			marginLeft: 0,
			marginRight: 0,
			printBackground: true,
			generateTaggedPDF: true,
			transferMode: 'ReturnAsStream'
		} as Protocol.Page.PrintToPDFRequest & { generateTaggedPDF: boolean });
		const pdfStream = this.session.readStream(cdpStream);
		// return new Promise((resolve, reject) => pdfStream.on("error", reject).on("end", resolve).pipe(outputStream));
		await postPDF(pdfStream, result, outputStream);
	}

	/** @deprecated */
	async getAccessibleString(elemId: string): Promise<string> {
		const {DOM, Accessibility} = this.session;
		await Promise.all([DOM, Accessibility].map((domain) => domain.enable({})));
		const {root: docNode} = await DOM.getDocument({});
		const {nodeId} = await DOM.querySelector({nodeId: docNode.nodeId, selector: '#' + elemId});
		const {nodes: axTree} = await Accessibility.queryAXTree({nodeId, role: 'StaticText'});
		if (!axTree.length) return null;
		return axTree.reduce((str, node) => str += node.name.value, "");
	}

	async getAccessibleStrings(elemIds: string[]): Promise<(string|null)[]> {
		const {DOM, Accessibility} = this.session;
		await Promise.all([DOM, Accessibility].map((domain) => domain.enable({})));
		const {root: docNode} = await DOM.getDocument({});
		const results: (string|null)[] = [];
		for (const elemId of elemIds) {
			const {nodeId} = await DOM.querySelector({nodeId: docNode.nodeId, selector: '#' + elemId});
			const {nodes: axTree} = await Accessibility.queryAXTree({nodeId, role: 'StaticText'});
			if (!axTree.length) results.push(null);
			else results.push(axTree.reduce((str, node) => str += node.name.value, ""));
		}
		await Promise.all([DOM, Accessibility].map((domain) => domain.disable()));
		return results;
	}
}
