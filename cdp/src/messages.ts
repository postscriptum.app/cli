import * as fs from "fs";
import * as path from 'path';
import * as os from 'os';
import {createCommandConfig, createLogConsole, createPreloadConfig, type ProcessResult} from "@postscriptum.app/cli-common";
import {CdpClient} from "./cdp";
import {CdpTarget} from "./target";
import type {Package} from "normalize-package-data";
import {DEFAULT_CHROMIUM_PORT, fetchVersionMetadata} from "./chromium";

const fsp = fs.promises;

export interface Message {
	type: string;
	tabId: number;
}

export interface ProcessRequest extends Message {
	type: 'process';
	targetId: string;
	plugins?: string[];
}

export interface ProcessResponse extends Message {
	type: 'processed';
}

export interface OutputPdfRequest extends Message {
	type: 'outputPdf';
	targetId: string;
}

export interface OutputPdfResponse extends Message {
	type: 'pdfOutput';
	url: string;
}

export class ErrorMessage {
	type: 'error';

	constructor(public message: string, public stack?: string, public tabId?: number) {
		this.type = 'error';
	}
}

export type InMessage = ProcessRequest | OutputPdfRequest;
export type OutMessage = ProcessResponse | OutputPdfResponse | ErrorMessage;

let tempDir: string;

const coreModulePath = require.resolve('@postscriptum.app/core');
const corePath = path.dirname(path.dirname(coreModulePath));

const targets: Record<string, CdpTarget> = {};
const results: Record<string, ProcessResult> = {};
const outputs: Record<string, string> = {};

if (process.argv.length == 3 && process.argv[2] == '--register') register(__filename);
else nativeMessaging();

async function nativeMessaging(): Promise<void> {
	let payloadSize: number | null = null;
	const chunks: Uint8Array[] = [];

	const {webSocketDebuggerUrl} = await fetchVersionMetadata(DEFAULT_CHROMIUM_PORT);
	const client = await CdpClient.connect(webSocketDebuggerUrl);
	process.stdin.on('readable', async () => {
		let chunk;
		while ((chunk = process.stdin.read()) !== null) chunks.push(chunk);

		const buffer = Buffer.concat(chunks);

		if (payloadSize === null) payloadSize = buffer.readUInt32LE(0);
		if (buffer.length >= (payloadSize + 4)) {
			const msg = JSON.parse(buffer.slice(4, (payloadSize + 4)).toString());
			payloadSize = null;
			chunks.splice(0);
			sendMessage(await handleMessage(client, msg));
		}
	});

	process.stdin.on('close', () => {
		process.exit(0);
	});

	process.on('uncaughtException', (error) => {
		sendMessage({type: 'error', message: 'Native ' + error.message, stack: 'Native ' + error.stack});
		process.exit(1);
	});

	process.on('SIGINT', () => {
		process.exit(130);
	});

	process.on('exit', () => {
		if (tempDir) fs.rmSync(tempDir, {recursive: true});
	});

	tempDir = await fsp.mkdtemp(path.join(os.tmpdir(), 'postscriptum-messages-'));
}

function sendMessage(msg: OutMessage): void {
	const buffer = Buffer.from(JSON.stringify(msg));
	const header = Buffer.alloc(4);
	header.writeUInt32LE(buffer.length, 0);
	process.stdout.write(Buffer.concat([header, buffer]));
}

async function handleMessage(cdpClient: CdpClient, msg: InMessage): Promise<OutMessage> {
	try {
		const {type, tabId} = msg;
		let target = targets[tabId];
		if (type == "process") {
			const plugins: Record<string, null | object> = {};
			if (msg.plugins) for (const pluginName of msg.plugins) plugins[pluginName] = null;
			const pluginsPaths = [path.resolve(corePath, `dist/plugins/`)];
			const commandConfig = await createCommandConfig({corePath, plugins, pluginsPaths});
			const preloadConfig = await createPreloadConfig(commandConfig, false);
			const logConsole = createLogConsole(path.resolve(tempDir, `${msg.tabId}.log`));

			if (!target) {
				target = new CdpTarget({
					cdpClient,
					targetId: msg.targetId,
					pipeConsole: false,
					corePath,
					pluginsPaths: pluginsPaths,
					preloadConfig,
					logConsole
				});
				await target.prepared;
			}
			targets[tabId] = target;

			results[tabId] = await target.process();

			return {
				type: 'processed',
				tabId
			};
		} else if (type == "outputPdf") {
			const target = targets[tabId];
			const result = results[tabId];
			if (!target || !result) return new ErrorMessage(`The page has not been processed`, null, msg.tabId);
			const output = path.resolve(tempDir, `${msg.tabId}.pdf`);
			await target.pdf(fs.createWriteStream(output), result);
			outputs[tabId] = output;
			return {
				type: 'pdfOutput',
				tabId,
				url: 'file:///' + output.replace(new RegExp('\\' + path.sep, 'g'), '/')
			};
		}
	} catch (error) {
		return new ErrorMessage('Target ' + error.message, 'Target ' + error.stack, msg.tabId);
	}
}

async function register(scriptPath: string): Promise<void> {
	try {
		const win32 = process.platform == "win32";
		const darwin = process.platform == "darwin";
		const pkgData: Package = require(`${__dirname}/../package.json`);
		const version = pkgData.version;
		const [majorVers, mediumVers] = version.split('.');

		const mediumVersion = `${majorVers}.${mediumVers}`;

		const msgManifest = {
			name: `app.postscriptum.messages_${mediumVersion}`,
			path: null as string,
			description: "Native messaging with the postscriptum cli interface",
			type: "stdio",
			// eslint-disable-next-line @typescript-eslint/naming-convention
			allowed_origins: ["chrome-extension://iannnbdlkeogdclhfjkalnlmofpjdjhp/"]
		};

		let psDataPath: string, chromeUserDataPath: string, chromiumUserDataPath: string;
		if (win32) {
			const configDir = process.env['APPDATA'];
			psDataPath = `${configDir}/Postscriptum`;
			chromeUserDataPath = `${configDir}/Google/Chrome`;
			chromiumUserDataPath = `${configDir}/Chromium`;
		} else if (darwin) {
			const configDir = `${os.homedir()}/Library/Application Support`;
			psDataPath = `${configDir}/Postscriptum`;
			chromeUserDataPath = `${configDir}/Google/Chrome`;
			chromiumUserDataPath = `${configDir}/Chromium`;
		} else {
			const configDir = `${os.homedir()}/.config`;
			psDataPath = `${configDir}/postscriptum`;
			chromeUserDataPath = `${configDir}/google-chrome`;
			chromiumUserDataPath = `${configDir}/chromium`;
		}
		const psAppUserDataPath = path.resolve(psDataPath, `UserData_${mediumVersion}`);

		await fsp.mkdir(psDataPath, {recursive: true});
		let batchPath: string;
		if (win32) {
			batchPath = path.resolve(psDataPath, `${msgManifest.name}.bat`);
			await fsp.rm(batchPath, {recursive: true, force: true});
			await fsp.writeFile(batchPath, `@echo off\n"${process.execPath}" "${scriptPath}"`);
		} else {
			batchPath = path.resolve(psDataPath, `${msgManifest.name}.sh`);
			await fsp.rm(batchPath, {recursive: true, force: true});
			await fsp.writeFile(batchPath, `#!/usr/bin/env bash\n"${process.execPath}" "${scriptPath}"`, {mode: 0o776});
		}
		msgManifest.path = batchPath;

		const msgManifestPaths: string[] = [];
		if (win32) {
			msgManifestPaths.push(path.resolve(psDataPath, `${msgManifest.name}.json`));
			await new Promise<void>((resolve, reject) => {
				const winreg = require('winreg');
				winreg({
					hive: winreg.HKCU,
					key: `\\Software\\Google\\Chrome\\NativeMessagingHosts\\${msgManifest.name}`
				}).set(winreg.DEFAULT_VALUE, winreg.REG_SZ, msgManifestPaths[0], (error: Error) => {
					if (error) reject(error);
					else resolve();
				});
			});
		} else {
			msgManifestPaths.push(`${chromeUserDataPath}/NativeMessagingHosts/${msgManifest.name}.json`);
			msgManifestPaths.push(`${chromiumUserDataPath}/NativeMessagingHosts/${msgManifest.name}.json`);
			msgManifestPaths.push(`${psAppUserDataPath}/NativeMessagingHosts/${msgManifest.name}.json`);
		}
		for (const msgManifestPath of msgManifestPaths) {
			await fsp.mkdir(path.dirname(msgManifestPath), {recursive: true});
			await fsp.writeFile(msgManifestPath, JSON.stringify(msgManifest));
		}
		console.log(`Native messaging '${msgManifest.name}' registered to ${scriptPath}`);
	} catch (e) {
		console.error(e.message);
	}
}
