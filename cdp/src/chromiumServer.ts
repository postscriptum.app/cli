import {chromiumSwitches, DEFAULT_CHROMIUM_PORT, spawnChrome} from "./chromium";
import {Command} from "commander";
import type {Package} from "normalize-package-data";

const pkgData: Package = require(`${__dirname}/../package.json`);
const argsParser = new Command()
	.command('postscriptum-chromium-server')
	.requiredOption('--chrome-path <path>', "The path to Chrome ('-' to not start the browser)")
	.option('--chrome-port <number>', "The port to listen (default to 9331)")
	.option('--headful', "Run the Chrome instance in headful mode.")
	.version(pkgData.version, '-v, --version', "Output the version number")
	.helpOption('-h, --help', 'Display help for command')
	.configureHelp({helpWidth: 80})
	.showHelpAfterError();
argsParser.parse();

const config = argsParser.opts();
const { chromePath, chromePort, headful } = config;

const chromeArgs = Array.from(chromiumSwitches);
const noSandbox = process.env['PS_NO_CHROMIUM_SANDBOX'];
if (noSandbox == "true" || noSandbox == "1") chromeArgs.push(`--no-sandbox`);
if (!headful) chromeArgs.push('--no-startup-window');

(async () => {
	await spawnChrome({
		chromePath,
		port: chromePort || DEFAULT_CHROMIUM_PORT,
		chromeArgs,
		tempUserData: true,
		logConsole: console,
		headless: !headful
	});
})();
