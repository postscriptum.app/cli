#!/usr/bin/env node
import * as fs from "node:fs";
import * as path from "node:path";
import * as console from "node:console";
import type {CommandConfig} from "@postscriptum.app/cli-common";
import {abortRace, createArgsParser, createCommandConfig, createLogConsole, createPreloadConfig} from "@postscriptum.app/cli-common";
import type {Package} from "normalize-package-data";
import {CdpTarget} from "./target";
import {CdpClient} from "./cdp";
import type {ChildProcess} from "node:child_process";
import type {Command} from "commander";
import {chromiumSwitches, fetchVersionMetadata, spawnChrome} from "./chromium";

export interface CdpCommandConfig extends CommandConfig {
	chromePath?: string;
	chromePort?: number | string;
}


class UsageError extends Error {

}

(async () => {
	let config: CdpCommandConfig = null;
	let logConsole: typeof console = null;
	let argsParser: Command;
	try {
		const pkgData: Package = require(`${__dirname}/../package.json`);
		argsParser = createArgsParser(pkgData.version, null, (argsParser) => {
			argsParser
				.option('--chrome-path <path>', "The path to Chrome ('-' to not start the browser)")
				.option('--chrome-port <number|string>', "The port to listen (9331 on dev mode)");
		});

		const coreModulePath = require.resolve('@postscriptum.app/core');
		const corePath = path.dirname(path.dirname(coreModulePath));
		const pluginsPaths = [path.resolve(corePath, `dist/plugins`), process.cwd()];

		config = await createCommandConfig<CdpCommandConfig>({corePath, pluginsPaths, chromePath: undefined, chromePort: 0}, argsParser);
		logConsole = createLogConsole(config.logFile);
	} catch (e) {
		console.error(e);
		process.exitCode = 1;
		return;
	}
	let endPoint: string | ChildProcess;
	const devMode = !config.output;
	const {chromePort, chromePath} = config;
	try {
		if (!chromePort && !chromePath) throw new UsageError(`required options '--chrome-path <path>' or '--chrome-port <number>' not specified`);
		if (chromePort) {
			if (typeof chromePort == "string" && chromePort.startsWith('ws://')) {
				endPoint = chromePort;
			} else {
				try {
					({webSocketDebuggerUrl: endPoint} = await fetchVersionMetadata(chromePort));
				} catch (e) {
					throw new Error(`Unable to connect to browser on port ${chromePort}`, {cause: e});
				}
			}
		}
		if (!endPoint) {
			const chromeArgs = Array.from(chromiumSwitches);
			const noSandbox = process.env['PS_NO_CHROMIUM_SANDBOX'];
			if (noSandbox == "true" || noSandbox == "1") chromeArgs.push(`--no-sandbox`);
			if (devMode) chromeArgs.push('--auto-open-devtools-for-tabs');
			chromeArgs.push('--no-startup-window');

			endPoint = await spawnChrome({
				chromePath,
				pipe: true,
				chromeArgs,
				tempUserData: true,
				logConsole: config.logLevel == "trace" ? logConsole : undefined,
				headless: !devMode
			});
		}
	} catch (e) {
		if (e instanceof UsageError) {
			argsParser.error(e.message);
		} else {
			logConsole.error(e);
			process.exitCode = 1;
			return;
		}
	}

	let cdpClient: CdpClient;
	let target: CdpTarget;
	let timeout: NodeJS.Timeout;
	try {
		const controller = new AbortController();
		process.once('SIGINT', (code) => controller.abort(new Error(`Process interrupted by signal (${code})`)));
		if (config.timeout) timeout = setTimeout(() => {
			controller.abort(new Error(`Process interrupted by timeout (${config.timeout}s)`));
		}, config.timeout * 1000);

		cdpClient = new CdpClient(endPoint, {logConsole, logLevel: config.logLevel});

		target = new CdpTarget({
			cdpClient: cdpClient,
			pipeConsole: !devMode,
			watchScript: devMode,
			corePath: config.corePath,
			pluginsPaths: config.pluginsPaths,
			preloadConfig: await createPreloadConfig(config, !devMode),
			logConsole,
			isolatedFileAccess: config.isolatedFileAccess
		});

		cdpClient.on('error', (error) => controller.abort(error));
		target.on('error', (error) => controller.abort(error));

		await abortRace(controller.signal, (async (): Promise<void> => {
			await target.prepared;
			const result = await target.process(config.input);

			if (!devMode) {
				const outputStream = config.output == "-" ? process.stdout : fs.createWriteStream(config.output);
				await target.pdf(outputStream, result);
			}
		})());
	} catch (e) {
		logConsole.error(e);
		process.exitCode = 1;
	} finally {
		try {
			if (timeout) clearTimeout(timeout);
			if (!devMode && cdpClient) {
				await target?.close();
				if (typeof endPoint != "string" && !endPoint.exitCode) {
					if (config.logLevel == "trace") logConsole.debug("Closing browser by CDP");
					// eslint-disable-next-line @typescript-eslint/naming-convention
					const {Browser} = cdpClient;
					await Browser.close();
					await new Promise((resolve) => endPoint.on('close', resolve));
				}
				await cdpClient.close();
			}
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
		} catch (e) {
			// Exceptions on browser close are ignored
		}
	}
})();

