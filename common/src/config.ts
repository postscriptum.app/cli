import * as path from "path";
import * as url from 'url';
import * as fs from "fs";
import {Command, Option} from "commander";
import type {PreloadConfig} from "./preload";

const fsp = fs.promises;

export type CommandLogLevel = "error" | "warn" | "info" | "debug" | "trace";

export interface CommandConfig {
	input: string;
	output?: string;
	corePath: string;
	pluginsPaths?: string[];
	plugins: Record<string, null | object>;
	noAuthorCss: boolean;
	cssFiles: string[];
	logLevel?: CommandLogLevel;
	logFile?: string;
	timeout: number;
	isolatedFileAccess?: boolean;
}

export const baseConfig: CommandConfig = {
	input: null,
	output: null,
	corePath: null,
	pluginsPaths: [],
	plugins: {},
	noAuthorCss: false,
	cssFiles: [],
	logLevel: "warn",
	logFile: null,
	timeout: 300000,
	isolatedFileAccess: false
};


function collectOption(value: string, previous: string[]): string[] {
	return previous.concat([value]);
}

export function createArgsParser(version: string, argv?: string[], additionalOpts?: (argsParser: Command) => void): Command {
	const argsParser = new Command()
		.command('postscriptum')
		.argument('<input>', "XHTML or HTML input file or URI")
		.option('-o, --output <output>', "PDF output, opens the page in dev mode in the browser if ommited ('-' to output to stdout)")
		.option("-p, --plugin <name>[:<path>][:json]", "Name of a plugin to load. The path can be specified with the syntax <name:path>", collectOption, [])
		.option("--plugins-paths [paths]", "Separated list of directories to search for plugin")
		.option("-c, --config <json|file>", "Path or JSON of a configuration file")
		.option("--css <file>", "Path of a CSS to apply to the page", collectOption, [])
		.option("--isolated-file-access", "Forbids all access to files outside the document's base folder")
		.option("--no-author-css", "Removes the CSS of the page")
		.addOption(new Option('--log-level <level>', "Level of messages to display in the log").choices(['error', 'warn', 'info', 'debug', 'trace']))
		.option("--log-file <file>", "Path of the log file to use, by default to the console")
		.option('-t, --timeout <number>', "Timeout in seconds before which the process is killed", 300 as any as string);

	argsParser.configureOutput({
		outputError: (str, write) => write(`Error: ${str}`)
	});

	if (additionalOpts) additionalOpts(argsParser);
	argsParser
		.version(version, '-v, --version', "Output the version number")
		.helpOption('-h, --help', 'Display help for command')
		.configureHelp({helpWidth: 80})
		.showHelpAfterError();

	if (argv) argsParser.parse(argv, {from: "user"});
	else argsParser.parse();

	return argsParser;
}

export async function createCommandConfig<C extends CommandConfig = CommandConfig>(partialConfig?: Partial<C>, argsParser?: Command): Promise<C> {
	const config: C = Object.assign({}, baseConfig, partialConfig) as C;
	if (!argsParser) return config;

	const args = argsParser.processedArgs;
	const opts = argsParser.opts();

	if (opts.config) {
		let configPath = path.resolve(process.cwd(), opts.config);
		if (!(await fsp.stat(configPath)).isFile()) configPath = require.resolve(opts.config, {paths: [process.cwd()]});
		const configExt = path.extname(opts.config);
		const externalConfig = await import(configPath, configExt == '.json' ? {with: {type: 'json'}} : undefined);
		Object.assign(config, externalConfig.default);
	}
	for (const key in config) {
		if (key in opts) (config as any)[key] = opts[key];
	}

	config.input = urlParam(args[0]);
	if (opts.timeout) config.timeout = parseInt(opts.timeout);

	if ('pluginsPaths' in opts) {
		if (!opts.pluginsPaths) config.pluginsPaths = null;
		else {
			config.pluginsPaths = opts.pluginsPaths.split(path.delimiter).map((pluginPath: string) => {
				return path.isAbsolute(pluginPath) ? pluginPath : require.resolve(pluginPath, {paths: require.resolve.paths(pluginPath).concat(process.cwd())});
			});
		}
	}

	if (opts.plugin) {
		for (const plugin of opts.plugin) {
			const confSep = plugin.indexOf(':');
			if (confSep == -1) config.plugins[plugin] = null;
			else {
				const pluginName = plugin.substring(0, confSep);
				const pluginConf = plugin.substring(confSep + 1);
				try {
					config.plugins[pluginName] = JSON.parse(pluginConf);
				} catch (e) {
					throw new Error(`Unable to parse the configuration of plugin '${pluginName}': ${pluginConf}`);
				}
			}
		}
	}

	if (opts.css) for (const cssPath of opts.css) config.cssFiles.push(path.resolve(process.cwd(), cssPath));

	config.noAuthorCss = !opts.authorCss;

	if (config.logLevel == "debug") console.log("Command line config:", JSON.stringify(config));
	return config as C;
}

function urlParam(pathOrUrl: string): string {
	const parsedUrl = url.parse(pathOrUrl);
	if (!parsedUrl.protocol || parsedUrl.protocol.length == 2 /* c:, d:, etc... */) {
		if (!path.isAbsolute(pathOrUrl)) pathOrUrl = path.resolve(pathOrUrl);
		if (!fs.existsSync(pathOrUrl)) throw new Error(`File '${pathOrUrl}' not found`);
		return 'file:' + pathOrUrl;
	}
	return pathOrUrl;
}

export async function createPreloadConfig(config: CommandConfig, pipeConsole: boolean): Promise<PreloadConfig> {
	const [coreScript, pluginsScripts] = await Promise.all([loadCoreScript(config.corePath), loadPluginsScripts(config.pluginsPaths)]);

	const cssTexts: string[] = [];
	for (const cssFile of config.cssFiles) {
		cssTexts.push(await fsp.readFile(path.resolve(process.cwd(), cssFile), "utf8"));
	}

	return {
		coreScript,
		pluginsScripts,
		pipeConsole,
		noAuthorCSS: config.noAuthorCss,
		cssTexts,
		defaultOptions: {},
		options: {
			plugins: config.plugins,
			logLevel: config.logLevel,
		}
	};
}

export async function createPreloadSource(preloadConfig: PreloadConfig, preloadScript?: string): Promise<string> {
	if (!preloadScript) preloadScript = await loadPreloadScript();
	// TODO preload.ts is transpiled as commonjs
	return `window.exports = {}; window.postscriptumPreload = ${JSON.stringify(preloadConfig)}; ${preloadScript}`;
}

export async function loadCoreScript(corePath: string): Promise<string> {
	const scriptPath = path.resolve(`${corePath}/dist/postscriptum.js`);
	const scriptMapPath = path.resolve(`${corePath}/dist/postscriptum.js.map`);

	let script = await fsp.readFile(require.resolve(scriptPath), 'utf-8');
	if (script.length && scriptMapPath) {
		const scriptMap = (await fsp.readFile(require.resolve(scriptMapPath))).toString('base64');
		script = script + "//# sourceMappingURL=data:application/json;charset=utf8;base64," + scriptMap;
	}
	return script;
}


export function watchCoreScript(corePath: string, watcher: (script: string) => void): void {
	const scriptPath = path.resolve(`${corePath}/dist/postscriptum.js`);

	let timeout: ReturnType<typeof setTimeout>;
	fs.watch(scriptPath, {persistent: false}, async () => {
		clearTimeout(timeout);
		timeout = setTimeout(async () => {
			const script = await loadCoreScript(corePath);
			if (script.length) watcher(script);
		}, 100);
	});
}

export function loadPlugin(pluginName: string, pluginsPaths: null | string[]): Promise<string> {
	let pluginPath = null;
	if (pluginsPaths) for (const pluginsPath of pluginsPaths) {
		const testPluginPath = path.resolve(pluginsPath, `${pluginName}.js`);
		if (fs.existsSync(testPluginPath)) {
			pluginPath = testPluginPath;
			break;
		}
	}

	if (!pluginPath) throw new Error(`Plugin '${pluginName}' not found.`);
	return fsp.readFile(pluginPath, 'utf-8');
}

export async function loadPluginsScripts(pluginsPaths: null | string[]): Promise<string[]> {
	const pluginsContents: string[] = [];
	if (pluginsPaths) for (let pluginPath of pluginsPaths) {
		if (!path.isAbsolute(pluginPath)) pluginPath = require.resolve(pluginPath, {paths: require.resolve.paths(pluginPath).concat(process.cwd())});
		const stat = await fsp.stat(pluginPath);
		if (stat.isFile()) pluginsContents.push(await fsp.readFile(pluginPath, 'utf-8'));
	}
	return pluginsContents;
}

export async function loadPreloadScript(): Promise<string> {
	return fsp.readFile(`${__dirname}/preload.js`, 'utf-8');
}

export async function loadHyph(lang: string, corePath: string): Promise<string> {
	if (!lang.match(/^[a-z]{2,3}(-[a-z]{2})?$/)) throw new Error("Invalid lang");
	return await fsp.readFile(path.resolve(corePath, 'dist', 'hyph', lang + '.json'), 'utf8');
}

export function isBasedOn(href: string, docHref: string): boolean {
	if (href == docHref) return true;
	const baseUrl = new URL(".", docHref);
	const resolved = new URL(href, baseUrl).href;
	return resolved.startsWith(baseUrl.href);
}

export function abortRace<T>(signal: AbortSignal, promise: Promise<T>): Promise<T> {
	if (!signal) return promise;
	if (signal.aborted) return Promise.reject(signal.reason);
	return Promise.race([
		new Promise<T>((resolve, reject) => {
			signal.addEventListener('abort', () => reject(signal.reason));
		}),
		promise
	]);
}

export async function timeoutRace(process: () => Promise<void>, timeout: number): Promise<void> {
	if (!timeout) return process();
	else {
		const result = await Promise.race([process(), new Promise((resolve) => {
			setTimeout(() => resolve('timeout'), timeout * 1000);
		})]);
		if (result == 'timeout') {
			throw new Error(`Process interrupted by timeout (${timeout}s)`);
		}
	}
}

export function createLogConsole(logFile?: string): typeof console {
	if (!logFile) return new console.Console({ stdout: process.stdout, stderr: process.stderr, colorMode: true });
	try {
		const logStream = fs.createWriteStream(logFile, {flags: 'a'});
		return new console.Console({ stdout: logStream, colorMode: false });
	} catch (e) {
		console.error(`Unable to write to the log file '${logFile}'`);
		return console;
	}
}

export {PreloadConfig, ProcessResult, ErrorResult} from "./preload.js";

export const defaultFontFamilies = {
	standard: 'Times New Roman',
	fixed: 'Monospace',
	serif: 'Times New Roman',
	sansSerif: 'Arial',
	cursive: 'Comic Sans MS',
	fantasy: 'Impact',
};
