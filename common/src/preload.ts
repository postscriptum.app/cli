import type {Postscriptum} from "@postscriptum.app/core";
import type {Plugin, PluginRegistry} from "@postscriptum.app/core/dist/d.ts/process";
import type {PaginatorOptions, Size} from "@postscriptum.app/core/dist/d.ts/pages";

interface DocumentInfo {
	title: string;
	author?: string;
	subject?: string;
	keywords?: string;
	creator?: string;
	producer?: string;
}

type BookmarkState = 'open' | 'closed';

interface Bookmark {
	label: string;
	dest: string;
	state: BookmarkState;
	children?: Bookmark[];
}

export interface PreloadConfig {
	coreScript: string;
	pluginsScripts: string[];
	noAuthorCSS: boolean;
	cssTexts: string[];
	defaultOptions: Partial<PaginatorOptions>;
	options?: Partial<PaginatorOptions>;
	pipeConsole: boolean;
}

export interface ProcessResult {
	pageSizes: Size[];
	maxPageSize?: Size;
	bookmarks?: Bookmark[];
	info?: DocumentInfo;
}

export interface ErrorResult {
	type: 'error';
	message: string;
	stack: string;
}

declare global {
	const postscriptumPreload: PreloadConfig & {
		postscriptum: Postscriptum;
		transientPluginsRegistry: PluginRegistry;
		done: Promise<ProcessResult | ErrorResult>;
	};

	interface Document {
		createTreeWalker(root: Node, whatToShow?: number, filter?: (node: Node) => number, entityReferenceExpansion?: boolean): TreeWalker;
	}
}
postscriptumPreload.transientPluginsRegistry = {};
window.postscriptum = postscriptumPreload.postscriptum = {
	plugin: function <O = Record<string, unknown>> (name: string, initializer?: Plugin<O>, defaultOptions: O = null, script: HTMLScriptElement = document.currentScript as HTMLScriptElement): CallableFunction {
		if (!initializer) {
			if (name in postscriptumPreload.transientPluginsRegistry) return postscriptumPreload.transientPluginsRegistry[name].initializer;
			return null;
		}

		postscriptumPreload.transientPluginsRegistry[name] = {
			initializer,
			defaultOptions,
			scriptOptions: null,
			script: script
		};
	}
} as Postscriptum;

type Options = { [key: string]: any };

postscriptumPreload.done = (async () => {
	if (window != window.top) return;

	try {
		const docInteractive = new Promise<Event | void>((resolve) => {
			if (document.readyState != 'loading') resolve();
			else document.addEventListener('DOMContentLoaded', resolve);
		});

		// eslint-disable-next-line no-inner-declarations
		function loadScript(content: string): Promise<HTMLScriptElement> {
			return new Promise<HTMLScriptElement>((resolve, reject) => {
				const script = document.createElement('script');
				const blob = new Blob([content], {type: 'application/javascript'});
				script.src = URL.createObjectURL(blob);
				script.onload = () => resolve(script);
				script.onerror = () => reject(new Error(`Failed to load script.`));
				document.head.appendChild(script);
			});
		}

		const INFO_METAS: { [k in keyof Partial<DocumentInfo>]: string; } = {
			author: 'author',
			subject: 'description',
			keywords: 'keywords',
			creator: 'generator'
		};

		// eslint-disable-next-line no-inner-declarations
		function getInfo(): DocumentInfo {
			const info: DocumentInfo = {title: document.title};
			for (const key in INFO_METAS) {
				const meta = document.head.querySelector(`meta[name=${INFO_METAS[key as keyof DocumentInfo]}]`);
				if (meta) info[key as keyof DocumentInfo] = meta.getAttribute('content');
			}
			return info;
		}

		// eslint-disable-next-line no-inner-declarations
		function getPageSizes(dest: Element): ProcessResult {
			const pageSizes: Size[] = [];
			const maxPageSize: Size = [-Infinity, -Infinity];

			for (let page = dest.firstElementChild; page; page = page.nextElementSibling) {
				if (page.localName == 'ps-page') {
					const style = getComputedStyle(page);
					const width = parseFloat(style.width);
					const height = parseFloat(style.height);

					if (width > maxPageSize[0]) maxPageSize[0] = width;
					if (height > maxPageSize[1]) maxPageSize[1] = height;

					pageSizes.push([width, height]);
				}
			}
			return {
				pageSizes,
				maxPageSize
			};
		}

		// eslint-disable-next-line no-inner-declarations
		function getBookmarks(dest: Element): Bookmark[] {
			const bookmarksRoot = dest.querySelector('ps-bookmarks');
			const bookmarks: Bookmark[] = [];
			if (!bookmarksRoot) return bookmarks;
			const walker = document.createTreeWalker(bookmarksRoot, NodeFilter.SHOW_ELEMENT, (element: HTMLElement) => {
				if (element.localName == 'li') return NodeFilter.FILTER_ACCEPT;
				return NodeFilter.FILTER_SKIP;
			});
			if (!walker.nextNode()) return bookmarks;

			const stack = [bookmarks];
			let end = false;
			while (!end) {
				const element = walker.currentNode as Element;
				const bookmark: Bookmark = {
					label: element.firstElementChild.textContent,
					dest: element.firstElementChild.getAttribute('href').substr(1),
					state: element.lastElementChild && element.lastElementChild.getAttribute('ps-state') as BookmarkState || 'open'
				};
				stack[stack.length - 1].push(bookmark);
				if (walker.firstChild()) {
					stack.push(bookmark.children = []);
				} else {
					while (!(walker.nextSibling()) && !end) {
						stack.pop();
						end = !walker.parentNode();
					}
				}

			}
			return bookmarks;
		}

		// eslint-disable-next-line no-inner-declarations
		function mergeOptions<O extends Options>(target: Partial<O>, ...optionsList: Partial<O>[]): O {
			const options = optionsList.shift();
			if (options) {
				for (const key of Object.keys(options)) {
					if (options[key] && typeof options[key] == 'object' && !Array.isArray(options[key])) {
						const targetProp = target[key];
						if (targetProp !== null && target.hasOwnProperty(key) && typeof targetProp == 'object' && !Array.isArray(targetProp)) mergeOptions(target[key], options[key]);
						else target[key as keyof O] = Object.assign({}, options[key]);
					} else {
						target[key as keyof O] = options[key];
					}
				}
			}

			return optionsList.length ? mergeOptions(target, ...optionsList) as O : target as O;
		}

		await docInteractive;
		const pagePostscriptum = postscriptumPreload.postscriptum == postscriptum ? null : postscriptum;

		if (!pagePostscriptum) await loadScript(postscriptumPreload.coreScript);
		for (const pluginName in postscriptumPreload.pluginsScripts) {
			if (!postscriptum.plugin(pluginName) && !postscriptumPreload.postscriptum.plugin(pluginName)) {
				await loadScript(postscriptumPreload.pluginsScripts[pluginName]);
			}
		}
		for (const pluginName in postscriptumPreload.transientPluginsRegistry) {
			const plugin = postscriptumPreload.transientPluginsRegistry[pluginName as keyof PluginRegistry];
			postscriptum.plugin(pluginName, plugin.initializer, plugin.defaultOptions, plugin.script);
		}
		if (postscriptumPreload.noAuthorCSS) {
			debugger;
			for (const styleSheet of Array.from(document.styleSheets)) {
				const node = styleSheet.ownerNode;
				node?.remove();
			}
		}
		for (const cssText of postscriptumPreload.cssTexts) {
			const styleElem = document.createElement('style');
			styleElem.textContent = cssText;
			document.head.appendChild(styleElem);
		}

		if (!pagePostscriptum) mergeOptions(postscriptum.pages.Paginator.defaultOptions, postscriptumPreload.defaultOptions);
		const options = mergeOptions({}, postscriptumPreload.options, postscriptum.getOptions());
		const processor = postscriptum.pagination(options);

		await processor.start().ended;

		/* TODO 1.0 Cleanup compatibility with core 0.12 */
		const result = !('printSize' in processor as any) ? getPageSizes(processor.dest) : {
			pageSizes: processor.pages.map((page) => page.printSize)
		};
		result.bookmarks = getBookmarks(processor.dest);
		result.info = getInfo();
		return result;
	} catch (e) {
		if (!postscriptumPreload.pipeConsole) try {
			postscriptum.util.logger.error(e);
		} catch (e) {
			console.error(e);
		}
		const error: ErrorResult = {
			type: 'error',
			message: e.message,
			stack: e.stack
		};
		return error;
	}
})();
