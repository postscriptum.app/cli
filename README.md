# postscriptum-cli

The command line utility of Postscriptum.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Installation

*postscriptum-cli* is downloadable from the [release page](https://gitlab.com/postscriptum.app/cli/-/releases "Releases · Postscriptum / cli · GitLab") of the project on GitLab. It is recommended to use the application which embeds Chromium. It is also available on [npm](https://www.npmjs.com/package/@postscriptum.app/cli).

## Usage

```
Usage: postscriptum [input] [options]

input               HTML or XHTML input file

Options:
   -o, --output FILE
        
   -t, --timeout NUMBER
        Timeout before which the process is killed [120][0 on dev mode]
   -p NAME[:PATH], --plugin NAME[:PATH]
        Name and path of a plugin to load
   --log-level error|warn|info|debug
        
   --chrome-path PATH
        The path to Chrome ('-' to not start the browser) [auto]
   --chrome-port PORT
        The port to listen [auto] [9331 on dev mode]
   -v, --version
        Display the version
```


## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).

## License

[MIT](https://gitlab.com/postscriptum.app/cli/-/blob/master/LICENSE)
